<!doctype html>
<html lang="tr">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <title>Mğu</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type='text/css'>
    <link rel="shortcut icon" type="image/png" href="/img/green-logo.png"/>
    @yield('head')
</head>
<body>
<div id="app">
    <v-app id="app">
        <v-navigation-drawer fixed clipped v-model="drawer" app>
            <v-list dense>
                <v-subheader class="mt-3 grey--text text--darken-1">Genel</v-subheader>
                <v-list-tile :selected="false" to="/">
                    <v-list-tile-action><v-icon>home</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Anasayfa</v-list-tile-title></v-list-tile-content>
                </v-list-tile>

                <v-subheader class="mt-3 grey--text text--darken-1">Gözetmen İşlemleri</v-subheader>
                <v-list-tile :to="{name: 'gozetmenIndex'}">
                    <v-list-tile-action><v-icon>supervisor_account</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Gözetmenler</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile :to="{name: 'gozetmenTuruIndex'}">
                    <v-list-tile-action><v-icon>toc</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Gözetmen Türleri</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile href="/panel/gozetmen-atama">
                    <v-list-tile-action><v-icon>assignment_turned_in</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Gözetmen Atama</v-list-tile-title></v-list-tile-content>
                </v-list-tile>

                <v-subheader class="mt-3 grey--text text--darken-1">Ders İşlemleri</v-subheader>
                <v-list-tile :to="{name: 'dersIndex'}">
                    <v-list-tile-action><v-icon>chrome_reader_mode</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Dersler</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile href="/">
                    <v-list-tile-action><v-icon>assignment_turned_in</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Ders Atama</v-list-tile-title></v-list-tile-content>
                </v-list-tile>

                <v-subheader class="mt-3 grey--text text--darken-1">Yerleşke / Sınav Alanı İşlemleri</v-subheader>
                <v-list-tile :to="{name: 'yerleskeIndex'}">
                    <v-list-tile-action><v-icon>location_city</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Yerleşkeler</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile :to="{name: 'sinavAlaniIndex'}">
                    <v-list-tile-action><v-icon>layers</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Sınav Alanları</v-list-tile-title></v-list-tile-content>
                </v-list-tile>

                <v-subheader class="mt-3 grey--text text--darken-1">Okul / Bölüm / Sınıf İşlemleri</v-subheader>
                <v-list-tile :to="{name: 'okulIndex'}">
                    <v-list-tile-action><v-icon>account_balance</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Okullar</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile :to="{name: 'bolumTuruIndex'}">
                    <v-list-tile-action><v-icon>library_books</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Bölüm Türleri</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile :to="{name: 'bolumIndex'}">
                    <v-list-tile-action><v-icon>library_books</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Bölümler</v-list-tile-title></v-list-tile-content>
                </v-list-tile>
                <v-list-tile :to="{name: 'sinifIndex'}">
                    <v-list-tile-action><v-icon>class</v-icon></v-list-tile-action>
                    <v-list-tile-content><v-list-tile-title>Sınıflar</v-list-tile-title></v-list-tile-content>
                </v-list-tile>

                <v-subheader class="mt-3 grey--text text--darken-1">Sistem Ayarları</v-subheader>
                <v-list-tile @click="">
                    <v-list-tile-action>
                        <v-icon color="grey darken-1">settings</v-icon>
                    </v-list-tile-action>
                    <v-list-tile-title class="grey--text text--darken-1">Genel Ayarlar</v-list-tile-title>
                </v-list-tile>
            </v-list>
        </v-navigation-drawer>
        <v-toolbar color="primary white--text" dense fixed clipped-left app>
            <v-toolbar-side-icon color="white--text" @click.stop="drawer = !drawer"></v-toolbar-side-icon>
            <v-avatar>
                <img src="/img/white-logo.svg" alt="logo" style="padding: 0.5rem">
            </v-avatar>
            <v-toolbar-title class="mr-5 align-center">
                <span class="title">Mğu</span>
            </v-toolbar-title>
            <v-spacer></v-spacer>
            <v-layout row align-center>

            </v-layout>
        </v-toolbar>
        <v-content>
            @section("content")
                <h1>Hoşgeldiniz.</h1>
            @show
        </v-content>
    </v-app>
</div>
@section("js")
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        new Vue({
            el: '#app',
            data: () => ({
                drawer: true,
            }),
        })
    </script>
@show
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
</body>
</html>