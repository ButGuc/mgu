@extends("panel.layouts.main")
@section("content")
    <v-container>
        <router-view></router-view>
    </v-container>
@endsection
@section("js")
    <script src="{{ asset('js/app.js') }}"></script>
@endsection