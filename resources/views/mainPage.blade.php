@extends("layouts.main")

@section("content")
    <div id="app">
        <v-app id="mainApp">
            <v-container grid-list-xl text-xs-center>
                <v-layout row wrap>
                    <v-flex xs12 md8 sm12 offset-md2>
                        <v-toolbar color="light-blue darken-4" dense dark>
                            <v-toolbar-title>Sınav Otomasyonu</v-toolbar-title>
                            <v-spacer></v-spacer>
                            <v-btn icon @click.native.stop="helpDialog = true">
                                <v-icon>help_outline</v-icon>
                            </v-btn>
                            <v-btn icon>
                                <v-icon>info_outline</v-icon>
                            </v-btn>
                            <v-dialog v-model="helpDialog" max-width="290">
                                <v-card>
                                    <v-card-title class="headline">Kullanıcı hesabım yok ne yapmam lazım ?</v-card-title>
                                    <v-card-text>
                                        Kullanıcı hesabınızı umit_gedik16@erdogan.edu.tr adresine, resmi e posta adresiniz ile ulaşmanız durumunda size hesabınız sunulacaktır.
                                    </v-card-text>
                                    <v-card-actions>
                                        <v-spacer></v-spacer>
                                        <v-btn color="green darken-1" flat="flat" @click.native="helpDialog = false">Tamam</v-btn>
                                    </v-card-actions>
                                </v-card>
                            </v-dialog>
                        </v-toolbar>
                    </v-flex>
                </v-layout>

                <v-layout row wrap>
                    <v-flex d-flex xs12 sm6 md4 lg5 offset-md2>
                        <v-card color="white" light>
                            <v-card-text class="yukari-cek">
                                <v-select class="asagi-indir"
                                          :items="items"
                                          label="Okul"
                                          item-value="Okul"
                                ></v-select>
                                <v-select
                                        :items="items"
                                        label="Bölüm"
                                        item-value="Bölüm"
                                ></v-select>
                                <v-select
                                        :items="items"
                                        label="Sınıf"
                                        item-value="Sınıf"
                                ></v-select>
                            </v-card-text>
                        </v-card>
                    </v-flex>
                    <v-flex d-flex xs12 sm6 md4 lg3 child-flex>
                        <v-card color="white" light>
                            <v-card-text>
                                <v-form>
                                    <v-text-field prepend-icon="person" name="login" label="E-Posta" type="text"></v-text-field>
                                    <v-text-field prepend-icon="lock" name="password" label="Parola" id="password" type="password"></v-text-field>
                                    <v-spacer></v-spacer>
                                    <v-btn flat color="green">Giriş Yap <v-icon right >account_circle</v-icon></v-btn>
                                    <v-btn flat color="red">Şifremi Unuttum <v-icon right >vpn_key</v-icon></v-btn>
                                </v-form>
                            </v-card-text>
                        </v-card>
                    </v-flex>
                </v-layout>
            </v-container>
        </v-app>
    </div>
@endsection

@section("js")
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                helpDialog: false,
                e1: null,
                items: [
                    {text: 'State 1'},
                    {text: 'State 2'},
                    {text: 'State 3'}
                ]
            }
        });
    </script>
@endsection