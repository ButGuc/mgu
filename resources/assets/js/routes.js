export default [
    //Home
    {path: '/', component: require('./components/PanelIndexComponent'), name: 'panelIndex'},
    {path: '/deneme', component: require('./components/DenemeComponent'), name: 'Deneme'},

    //Yerleşke
    {path: '/yerleske/:alert?', component: require('./components/mainPanel/yerleske/IndexComponent'), name: 'yerleskeIndex'},
    {path: '/yerleske/ekle/:status?', component: require('./components/mainPanel/yerleske/EkleComponent'), name: 'yerleskeEkle'},
    {path: '/yerleske/duzenle/:id/:status?', component: require('./components/mainPanel/yerleske/DuzenleComponent'), name: 'yerleskeDuzenle'},

    //sinav alani
    {path: '/sinavalani/:alert?', component: require('./components/mainPanel/sinavAlani/IndexComponent'), name: 'sinavAlaniIndex'},
    {path: '/sinavalani/ekle/:status?', component: require('./components/mainPanel/sinavAlani/EkleComponent'), name: 'sinavAlaniEkle'},
    {path: '/sinavalani/duzenle/:id/:status?', component: require('./components/mainPanel/sinavAlani/DuzenleComponent'), name: 'sinavAlaniDuzenle'},

    //Okul
    {path: '/okul/:alert?', component: require('./components/mainPanel/okul/IndexComponent'), name: 'okulIndex'},
    {path: '/okul/ekle/:status?', component: require('./components/mainPanel/okul/EkleComponent'), name: 'okulEkle'},
    {path: '/okul/duzenle/:id/:status?', component: require('./components/mainPanel/okul/DuzenleComponent'), name: 'okulDuzenle'},

    //Bölüm Türü
    {path: '/bolumturu/:alert?', component: require('./components/mainPanel/bolumTuru/IndexComponent'), name: 'bolumTuruIndex'},
    {path: '/bolumturu/ekle/:status?', component: require('./components/mainPanel/bolumTuru/EkleComponent'), name: 'bolumTuruEkle'},
    {path: '/bolumturu/duzenle/:id/:status?', component: require('./components/mainPanel/bolumTuru/DuzenleComponent'), name: 'bolumTuruDuzenle'},

    //Bölüm
    {path: '/bolum/:alert?', component: require('./components/mainPanel/bolum/IndexComponent'), name: 'bolumIndex'},
    {path: '/bolum/ekle/:status?', component: require('./components/mainPanel/bolum/EkleComponent'), name: 'bolumEkle'},
    {path: '/bolum/duzenle/:id/:status?', component: require('./components/mainPanel/bolum/DuzenleComponent'), name: 'bolumDuzenle'},

    //Sinif
    {path: '/sinif/:alert?', component: require('./components/mainPanel/sinif/IndexComponent'), name: 'sinifIndex'},
    {path: '/sinif/ekle/:status?', component: require('./components/mainPanel/sinif/EkleComponent'), name: 'sinifEkle'},
    {path: '/sinif/duzenle/:id/:status?', component: require('./components/mainPanel/sinif/DuzenleComponent'), name: 'sinifDuzenle'},

    //Ders
    {path: '/ders/:alert?', component: require('./components/mainPanel/ders/IndexComponent'), name: 'dersIndex'},
    {path: '/ders/ekle/:status?', component: require('./components/mainPanel/ders/EkleComponent'), name: 'dersEkle'},
    {path: '/ders/duzenle/:id/:status?', component: require('./components/mainPanel/ders/DuzenleComponent'), name: 'dersDuzenle'},

    //Gozetmen Turu
    {path: '/gozetmenturu/:alert?', component: require('./components/mainPanel/gozetmenTuru/IndexComponent'), name: 'gozetmenTuruIndex'},
    {path: '/gozetmenturu/ekle/:status?', component: require('./components/mainPanel/gozetmenTuru/EkleComponent'), name: 'gozetmenTuruEkle'},
    {path: '/gozetmenturu/duzenle/:id/:status?', component: require('./components/mainPanel/gozetmenTuru/DuzenleComponent'), name: 'gozetmenTuruDuzenle'},

    //Gozetmen
    {path: '/gozetmen/:alert?', component: require('./components/mainPanel/gozetmen/IndexComponent'), name: 'gozetmenIndex'},
    {path: '/gozetmen/ekle/:status?', component: require('./components/mainPanel/gozetmen/EkleComponent'), name: 'gozetmenEkle'},
    {path: '/gozetmen/duzenle/:id/:status?', component: require('./components/mainPanel/gozetmen/DuzenleComponent'), name: 'gozetmenDuzenle'},
    {path: '/gozetmen/izin/:id/', component: require('./components/mainPanel/gozetmen/IzınComponent'), name: 'gozetmenIzın'},
]