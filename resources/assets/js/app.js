import Vuetify from 'vuetify';
import VueRouter from 'vue-router';

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.Vue = require('vue');

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

Vue.use(Vuetify, {
    theme: {
        primary: "#43A047",
        secondary: "#388E3C",
        accent: "#388E3C",
        success: "#66BB6A",
        error: "#FF1744",
        warning: "#FB8C00"
    },
    options: {
        themeVariations: [
            'primary',
            'secondary'
        ]
    }
});

Vue.use(VueRouter);

const routes = require('./routes.js').default;

const router = new VueRouter({routes});

const app = new Vue({ router, el: '#app', data: () => ({ drawer: true, }), }).$mount('#app');