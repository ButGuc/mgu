import tanimliMesajlarMixin from '../../mixins/tanimliMesajlarMixin'
export default {
    mixins: [ tanimliMesajlarMixin ],
    data () {
        return {
            valid: true,
            loading: false,

            alertMessage: null,
            alertStatus: false,
            alertType: 'success',
        }
    },
    mounted() {
        let app = this;

        app.selfID = app.$route.params.id;
        app.loading = true;

        axios.get('/api/v1/'+ app.apiResource +'/' + app.selfID)
            .then(function (resp) {
                app.selfData = resp.data;
                app.swap();
                app.loading = false;
            })
            .catch(function () {
                app.loading = false;
                app.$router.push({name: app.placeToReturn, params: {alert: {alertMessage: app.tanimliMesajlar.agHatasi, alertType: "error"}}});
            });

        for (let key in app.gerekliVeriler) {
            app.loading = true;
            axios.get('/api/v1/'+app.gerekliVeriler[key].apiResource)
                .then(function (resp) {
                    app.gerekliVeriler[key].data = resp.data;
                    if (resp.data <= 0) {
                        app.$router.push({name: app.gerekliVeriler[key].errorReturn, params: {alert: {alertMessage: app.gerekliVeriler[key].errorMessages, alertType: "error"}}});
                    }
                    app.loading = false;
                })
                .catch(function () {
                    app.loading = false;
                    app.$router.push({name: app.placeToReturn, params: {alert: {alertMessage: app.tanimliMesajlar.agHatasi, alertType: "error"}}});
                });
        }
    },
    methods: {
        submit () {
            if (this.$refs.form.validate()) {
                let app = this;

                app.loading = true;

                axios.patch('/api/v1/'+app.apiResource+'/'+app.selfID, app.veriler,)
                    .then(function (resp) {
                        app.loading = false;
                        app.$router.push({name: app.placeToReturn, params: {alert: {alertMessage: app.tanimliMesajlar.duzenlendi, alertType: "success"}}});
                    })
                    .catch(function (resp) {
                        console.log(resp);
                        app.loading = false;
                        app.alertStatus = true; app.alertMessage = app.tanimliMesajlar.agHatasi; app.alertType = 'error'
                    });
            }
        },
        clear () {
            this.$refs.form.reset();
        },
        deleteFile(self) {
            this.files[self].filename = '';
            this.files[self].url = '';
            this.files[self].file = '';
            this.veriler[self] = false;
        },
        pickFile () {
            this.$refs.image.click ()
        },
        onFileChange(e, self) {
            let files = e.target.files || e.dataTransfer.files;
            if (!files.length) {
                this.files[self].filename = '';
                this.files[self].url = '';
                this.files[self].file = '';
                this.veriler[self] = null;
                return;
            }
            this.files[self].deleteit = false;
            this.files[self].filename = files[0].name;
            this.files[self].file = files[0];
            this.createFile(files[0], self);

            const fr = new FileReader ();
            fr.readAsDataURL(files[0]);
            fr.addEventListener('load', () => {
                this.files[self].url = fr.result;
            });
        },
        createFile(file, self) {
            let reader = new FileReader();
            let vm = this;
            reader.onload = (e) => {
                vm.veriler[self] = e.target.result;
            };
            reader.readAsDataURL(file);
        },
    },
}