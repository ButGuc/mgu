import tanimliMesajlarMixin from '../../mixins/tanimliMesajlarMixin'
export default {
    mixins: [ tanimliMesajlarMixin ],
    data () {
        return {
            loading: true,

            detailDialog: false,
            detailDialogSelected: null,

            deleteDialog: false,
            deleteDialogSelected: null,

            alertMessage: null,
            alertStatus: false,

            aranan: '',
            veriler: [],

            alertType: 'success',
        }
    },
    mounted() {
        let app = this;

        if (app.$route.params.alert !== undefined && app.$route.params.alert.alertMessage !== undefined && app.$route.params.alert.alertType !== undefined) {
            app.alertStatus = true; app.alertMessage = app.$route.params.alert.alertMessage; app.alertType = app.$route.params.alert.alertType;
        } else {
            app.alertStatus = false;
        }

        axios.get('/api/v1/'+app.apiResource)
            .then(function (resp) {
                app.veriler = resp.data;
                app.loading = false;
            })
            .catch(function (resp) {
                app.loading = false;
                app.alertStatus = true; app.alertMessage = app.tanimliMesajlar.agHatasi; app.alertType = "error";
            });
    },
    methods: {
        detailDialogOpen(item) {
            let app = this;

            app.loading = true;

            axios.get('/api/v1/'+app.apiResource+'/'+ item.id)
                .then(function (resp) {
                    app.detailDialogSelected = resp.data;
                    app.loading = false;
                    app.detailDialog = true;
                })
                .catch(function (resp) {
                    app.loading = false;
                    app.alertStatus = true; app.alertMessage = app.tanimliMesajlar.agHatasi; app.alertType = "error";
                });
        },
        deleteDialogOpen(item) {
            let app = this;

            app.deleteDialogSelected = item;
            app.deleteDialog = true;
        },
        deleteEntry() {
            let app = this;

            app.loading = true;
            app.deleteDialog = false;

            const index = this.veriler.indexOf(app.deleteDialogSelected);
            const id = app.deleteDialogSelected.id;

            app.deleteDialog = false;
            app.deleteDialogSelected = null;

            axios.delete('/api/v1/'+app.apiResource+'/' + id)
                .then(function (resp) {
                    app.veriler.splice(index, 1);
                    app.alertStatus = true; app.alertMessage = app.tanimliMesajlar.silindi; app.loading = false;
                })
                .catch(function (resp) {
                    if (resp.message === "Network Error") {
                        app.alertStatus = true; app.alertMessage = app.tanimliMesajlar.agHatasi; app.alertType = "error"; app.loading = false;
                    } else if (resp.response.status === 500){
                        app.alertStatus = true; app.alertMessage = app.tanimliMesajlar.silmeHatasi; app.alertType = "error"; app.loading = false;
                    }
                });
        }
    }
}