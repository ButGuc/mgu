export default {
    data () {
        return {
            tanimliMesajlar: {
                agHatasi: "Sunucu veya Ağ hatası",
                silmeHatasi: "Öncelikle bağlı olduğu verileri silmeniz gerekiyor. (Vernin tüm ilişkili verilerini silmeniz gerekiyor.)",
                veriBulunamadi: "Veri Bulunamadı",
                eklendi: "Başarıyla Eklendi",
                duzenlendi: "Başarıyla Düzenlendi",
                kaydedildi: "Başarıyla Kaydedildi",
                silindi: "Başarıyla Silindi",
            }
        }
    }
}