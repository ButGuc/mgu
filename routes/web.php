<?php

Route::get('/', function () {
    return view('mainPage');
});

Route::prefix('/panel')->group(function () {
    Route::get('/', function () {
        return view('panel.main');
    });
});