<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1', 'namespace' => 'Api', 'as' => 'api.'], function () {
    Route::resource('yerleske', 'YerleskeController', ['except' => ['create', 'edit']]);
    Route::resource('sinavalani', 'SinavAlaniController', ['except' => ['create', 'edit']]);
    Route::resource('okul', 'OkulController', ['except' => ['create', 'edit']]);
    Route::resource('bolumturu', 'BolumTuruController', ['except' => ['create', 'edit']]);
    Route::resource('bolum', 'BolumController', ['except' => ['create', 'edit']]);
    Route::resource('sinif', 'SinifController', ['except' => ['create', 'edit']]);
    Route::resource('ders', 'DersController', ['except' => ['create', 'edit']]);
    Route::resource('gozetmenturu', 'GozetmenTuruController', ['except' => ['create', 'edit']]);
    Route::resource('gozetmen', 'GozetmenController', ['except' => ['create', 'edit']]);
    Route::resource('gozetmenizin', 'GozetmenIzinController', ['except' => ['create', 'edit', 'store', 'destroy']]);
});
