<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinifDers extends Model
{
    protected $fillable = ['ders_id','sinif_donem_id'];

    public function Ders() {
        return $this->belongsTo('App\Ders', 'ders_id');
    }

    public function SinifDonem() {
        return $this->belongsTo('App\SinifDonem', 'sinif_donem_id');
    }
}
