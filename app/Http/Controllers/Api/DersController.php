<?php

namespace App\Http\Controllers\Api;

use App\Ders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DersController extends Controller
{
    public function index()
    {
        return Ders::all();
    }

    public function show($id)
    {
        return Ders::findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = Ders::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = Ders::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        Ders::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
