<?php

namespace App\Http\Controllers\Api;

use App\Bolum;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BolumController extends Controller
{
    public function index()
    {
        return Bolum::with(['BolumTuru','Okul'])->get();
    }

    public function show($id)
    {
        return Bolum::with(['BolumTuru','Okul'])->findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = Bolum::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = Bolum::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        Bolum::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
