<?php

namespace App\Http\Controllers\Api;

use App\GozetmenTuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GozetmenTuruController extends Controller
{
    public function index()
    {
        return GozetmenTuru::all();
    }

    public function show($id)
    {
        return GozetmenTuru::findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = GozetmenTuru::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = GozetmenTuru::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        GozetmenTuru::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
