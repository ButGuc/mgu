<?php

namespace App\Http\Controllers\Api;

use App\Gozetmen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GozetmenIzinController extends Controller
{
    public function index()
    {
        return Gozetmen::all('id', 'gozetmen_izin');
    }

    public function show($id)
    {
        return Gozetmen::with(['Bolum','Bolum.Okul','GozetmenTuru'])->findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $obj = Gozetmen::findOrFail($id);
        $obj->gozetmen_izin = json_encode($request->get('izinler'));
        $obj->save();

        return response()->json($obj, 200);
    }
}
