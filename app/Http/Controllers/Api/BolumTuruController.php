<?php

namespace App\Http\Controllers\Api;

use App\BolumTuru;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BolumTuruController extends Controller
{
    public function index()
    {
        return BolumTuru::all();
    }

    public function show($id)
    {
        return BolumTuru::findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = BolumTuru::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = BolumTuru::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        BolumTuru::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
