<?php

namespace App\Http\Controllers\Api;

use App\Okul;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OkulController extends Controller
{
    public function index()
    {
        return Okul::with('Yerleske')->get();
    }

    public function show($id)
    {
        return Okul::with('Yerleske')->findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = Okul::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = Okul::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        Okul::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
