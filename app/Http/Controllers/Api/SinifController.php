<?php

namespace App\Http\Controllers\Api;

use App\Sinif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SinifController extends Controller
{
    public function index()
    {
        return Sinif::with(['Bolum','Bolum.BolumTuru','Bolum.Okul'])->get();
    }

    public function show($id)
    {
        return Sinif::with(['Bolum','Bolum.BolumTuru','Bolum.Okul'])->findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = Sinif::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = Sinif::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        Sinif::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
