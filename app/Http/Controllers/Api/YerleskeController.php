<?php

namespace App\Http\Controllers\Api;

use App\Yerleske;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class YerleskeController extends Controller
{
    public function index()
    {
        return Yerleske::all();
    }

    public function show($id)
    {
        return Yerleske::findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = Yerleske::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = Yerleske::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        Yerleske::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
