<?php

namespace App\Http\Controllers\Api;

use App\Gozetmen;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GozetmenController extends Controller
{
    public $gozetmen_resim_path = '/uploads/gozetmen_resim/';
    public $gozetmen_resim_thumb_path = '/uploads/gozetmen_resim/thumb/';

    public function index()
    {
        return Gozetmen::with(['Bolum','Bolum.Okul','GozetmenTuru'])->get();
    }

    public function show($id)
    {
        return Gozetmen::with(['Bolum','Bolum.Okul','GozetmenTuru'])->findOrFail($id);
    }

    public function store(Request $request)
    {
        $image = $request->get('gozetmen_resim');

        if($image)
        {
            $name = Str::uuid()->toString();

            Image::make($image)->resize(150, 150)->save(public_path($this->gozetmen_resim_path).$name);
            Image::make($image)->resize(25, 25)->save(public_path($this->gozetmen_resim_thumb_path).$name);

            $image = $name;
        }
        else if (!$image || $image == null)
        {
            $image = 'null';
        }

        $request->merge(['gozetmen_resim' => $image]);

        $obj = Gozetmen::create($request->all());

        return response()->json($obj, 200);
    }

    public function update(Request $request, $id)
    {
        $obj = Gozetmen::findOrFail($id);

        $image = $request->get('gozetmen_resim');

        if($image && $image != 'same')
        {
            if ($obj->gozetmen_resim != 'null')
            {
                @unlink(public_path().$this->gozetmen_resim_path.$obj->gozetmen_resim);
                @unlink(public_path().$this->gozetmen_resim_thumb_path.$obj->gozetmen_resim);
            }

            $name = Str::uuid()->toString();

            Image::make($image)->resize(150, 150)->save(public_path($this->gozetmen_resim_path).$name);
            Image::make($image)->resize(25, 25)->save(public_path($this->gozetmen_resim_thumb_path).$name);

            $image = $name;
        }
        else if ($image == 'same')
        {
            $image = $obj->gozetmen_resim;
        }
        else if ($image == false)
        {
            if ($obj->gozetmen_resim != 'null')
            {
                @unlink(public_path().$this->gozetmen_resim_path.$obj->gozetmen_resim);
                @unlink(public_path().$this->gozetmen_resim_thumb_path.$obj->gozetmen_resim);
            }

            $image = 'null';
        }

        $request->merge(['gozetmen_resim' => $image]);

        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        $obj = Gozetmen::findOrFail($id);

        if ($obj->gozetmen_resim != 'null') {
            @unlink(public_path().$this->gozetmen_resim_path.$obj->gozetmen_resim);
            @unlink(public_path().$this->gozetmen_resim_thumb_path.$obj->gozetmen_resim);
        }

        $obj->delete();

        return response()->json(null, 204);
    }
}
