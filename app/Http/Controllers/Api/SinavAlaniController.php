<?php

namespace App\Http\Controllers\Api;

use App\SinavAlani;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SinavAlaniController extends Controller
{
    public function index()
    {
        return SinavAlani::with('Yerleske')->get();
    }

    public function show($id)
    {
        return SinavAlani::with('Yerleske')->findOrFail($id);
    }

    public function store(Request $request)
    {
        $obj = SinavAlani::create($request->all());

        return response()->json($obj, 201);
    }

    public function update(Request $request, $id)
    {
        $obj = SinavAlani::findOrFail($id);
        $obj->update($request->all());

        return response()->json($obj, 200);
    }

    public function destroy($id)
    {
        SinavAlani::findOrFail($id)->delete();

        return response()->json(null, 204);
    }
}
