<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinavAlani extends Model
{
    protected $fillable = ['sinav_alani_adi','sinav_alani_kati','sinav_alani_kodu','sinav_alani_kapasitesi','sinav_alani_gerekli_gorevli_sayisi','yerleske_id'];

    public function Yerleske() {
        return $this->belongsTo('App\Yerleske', 'yerleske_id');
    }
}
