<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bolum extends Model
{
    protected $fillable = ['bolum_adi','bolum_turu_id','okul_id'];

    public function BolumTuru() {
        return $this->belongsTo('App\BolumTuru', 'bolum_turu_id');
    }

    public function Okul() {
        return $this->belongsTo('App\Okul', 'okul_id');
    }

    public function Sinif() {
        return $this->hasMany('App\Sinif','id');
    }

    public function Gozetmen() {
        return $this->hasMany('App\Gozetmen','id');
    }
}
