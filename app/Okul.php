<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Okul extends Model
{
    protected $fillable = ['okul_adi','yerleske_id'];

    public function Yerleske() {
        return $this->belongsTo('App\Yerleske', 'yerleske_id');
    }

    public function Bolum() {
        return $this->hasMany('App\Bolum','okul_id');
    }
}
