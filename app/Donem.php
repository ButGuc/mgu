<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donem extends Model
{
    protected $fillable = ['donem_adi'];
}
