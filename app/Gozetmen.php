<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gozetmen extends Model
{
    protected $fillable = ['gozetmen_adi', 'gozetmen_eposta','gozetmen_telefon','gozetmen_resim','gozetmen_izin','gozetmen_turu_id','bolum_id'];

    public function Bolum() {
        return $this->belongsTo('App\Bolum', 'bolum_id');
    }

    public function GozetmenTuru() {
        return $this->belongsTo('App\GozetmenTuru', 'gozetmen_turu_id');
    }
}
