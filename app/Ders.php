<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ders extends Model
{
    protected $fillable = ['ders_adi','ders_kodu'];

    public function SinifDers() {
        return $this->hasMany('App\SinifDers','ders_id');
    }
}
