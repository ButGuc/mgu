<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SinifDonem extends Model
{
    protected $fillable = ['sinif_id','donem_id'];

    public function Sinif() {
        return $this->belongsTo('App\Sinif', 'sinif_id');
    }

    public function Donem() {
        return $this->belongsTo('App\Donem', 'donem_id');
    }
}
