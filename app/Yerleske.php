<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yerleske extends Model
{
    protected $fillable = ['yerleske_adi','yerleske_adresi'];

    public function Okul() {
        return $this->hasMany('App\Okul','yerleske_id');
    }

    public function SinifAlani() {
        return $this->hasMany('App\SinifAlani','yerleske_id');
    }
}
