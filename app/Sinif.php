<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sinif extends Model
{
    protected $fillable = ['sinif_adi','bolum_id'];

    public function Bolum() {
        return $this->belongsTo('App\Bolum', 'bolum_id');
    }

    public function SinifDonem() {
        return $this->hasMany('App\SinifDonem','sinif_id');
    }
}
