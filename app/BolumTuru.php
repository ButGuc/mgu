<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BolumTuru extends Model
{
    protected $fillable = ['bolum_turu_adi'];

    public function Bolum() {
        return $this->hasMany('App\Bolum','bolum_turu_id');
    }
}
