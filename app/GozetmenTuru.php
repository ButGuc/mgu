<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GozetmenTuru extends Model
{
    protected $fillable = ['gozetmen_turu_adi', 'gozetmen_turu_gorev_limiti'];

    public function Gozetmen() {
        return $this->hasMany('App\Gozetmen','id');
    }
}
