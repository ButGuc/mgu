<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSinifDersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinif_ders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ders_id')->unsigned();
            $table->integer('sinif_donem_id')->unsigned();
            $table->foreign('ders_id')->references('id')->on('ders');
            $table->foreign('sinif_donem_id')->references('id')->on('sinif_donems');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinif_ders');
    }
}
