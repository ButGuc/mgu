<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSinavAlanisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinav_alanis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sinav_alani_adi');
            $table->string('sinav_alani_kati')->default("Zemin");
            $table->string('sinav_alani_kodu')->default("Tanımlı Değil");
            $table->integer('sinav_alani_kapasitesi');
            $table->integer('sinav_alani_gerekli_gorevli_sayisi');
            $table->integer('yerleske_id')->unsigned();
            $table->foreign('yerleske_id')->references('id')->on('yerleskes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinav_alanis');
    }
}
