<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGozetmenTurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gozetmen_turus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gozetmen_turu_adi');
            $table->integer('gozetmen_turu_gorev_limiti');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gozetmen_turus');
    }
}