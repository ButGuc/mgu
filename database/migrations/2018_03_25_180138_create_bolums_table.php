<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBolumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bolums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bolum_adi');
            $table->integer('bolum_turu_id')->unsigned();
            $table->foreign('bolum_turu_id')->references('id')->on('bolum_turus');
            $table->integer('okul_id')->unsigned();
            $table->foreign('okul_id')->references('id')->on('okuls');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bolums');
    }
}
