<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOkulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('okuls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('okul_adi');
            $table->integer('yerleske_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('okuls', function($table)
        {
            $table->foreign('yerleske_id')->references('id')->on('yerleskes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('okuls');
    }
}
