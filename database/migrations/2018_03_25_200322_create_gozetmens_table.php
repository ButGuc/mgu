<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGozetmensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gozetmens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gozetmen_adi');
            $table->string('gozetmen_eposta');
            $table->string('gozetmen_telefon');
            $table->string('gozetmen_resim');
            $table->json('gozetmen_izin')->nullable();
            $table->integer('gozetmen_turu_id')->unsigned();
            $table->integer('bolum_id')->unsigned();
            $table->foreign('gozetmen_turu_id')->references('id')->on('gozetmen_turus');
            $table->foreign('bolum_id')->references('id')->on('bolums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gozetmens');
    }
}
