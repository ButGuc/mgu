<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSinifDonemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinif_donems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sinif_id')->unsigned();
            $table->integer('donem_id')->unsigned();
            $table->foreign('sinif_id')->references('id')->on('sinifs');
            $table->foreign('donem_id')->references('id')->on('donems');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinif_donems');
    }
}

